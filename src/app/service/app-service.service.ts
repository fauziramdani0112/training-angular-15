//import module
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppServiceService {

  private baseUrl = 'http://localhost:3000/api/master/';

  constructor(private http: HttpClient) { }

  //film
  getAllFilms(): Observable<any[]> {
    return this.http.get<any[]>(`${this.baseUrl}film`);
  }
  insertFilm(obj: any): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}film`, obj);
  }
  updateFilm(id: number, obj: any): Observable<any> {
    return this.http.put<any>(`${this.baseUrl}film/${id}`, obj);
  }
  //actor
  getAllActor(): Observable<any[]> {
    return this.http.get<any[]>(`${this.baseUrl}actor`);
  }
  insertActor(obj: any): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}actor`, obj);
  }
  updateActor(id: number, obj: any): Observable<any> {
    return this.http.put<any>(`${this.baseUrl}actor/${id}`, obj);
  }
  //category
  getAllCategory(): Observable<any[]> {
    return this.http.get<any[]>(`${this.baseUrl}category`);
  }
  insertCategory(obj: any): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}category`, obj);
  }
  updateCategory(id: number, obj: any): Observable<any> {
    return this.http.put<any>(`${this.baseUrl}category/${id}`, obj);
  }
}
