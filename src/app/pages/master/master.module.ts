import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MasterActorComponent } from './master-actor/master-actor.component';
import { MasterCategoryComponent } from './master-category/master-category.component';
import { MasterFilmComponent } from './master-film/master-film.component';
import { MasterRoutingModule } from './master-routing.module';
import { MasterLanguageComponent } from './master-language/master-language.component';

//Angular MAterial
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import { ActorFormComponent } from './actor-form/actor-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    MasterActorComponent,
    MasterCategoryComponent,
    MasterFilmComponent,
    MasterLanguageComponent,
    ActorFormComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MasterRoutingModule,
    MatSlideToggleModule,
    MatFormFieldModule, MatInputModule, MatTableModule, MatSortModule, MatPaginatorModule, MatCardModule,MatButtonModule,MatDialogModule,MatDialogModule
  ]
})
export class MasterModule { }
