// actor-form.component.ts
import { Component, Inject, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef,MAT_DIALOG_DATA  } from '@angular/material/dialog';

@Component({
  selector: 'app-actor-form',
  templateUrl: './actor-form.component.html',
  styleUrls: ['./actor-form.component.css']
})
export class ActorFormComponent implements OnInit {
  
  actorForm!: FormGroup;
  constructor(public dialogRef: MatDialogRef<ActorFormComponent>, @Inject(MAT_DIALOG_DATA) public dataActor: any,private formBuilder: FormBuilder) {
    this.actorForm = this.formBuilder.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    if (this.dataActor) {
      this.actorForm.patchValue({
        first_name: this.dataActor.first_name,
        last_name: this.dataActor.last_name
      });
    }
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onSave(): void {
    if (this.actorForm.valid) {
      // Kirim nilai formulir ke dialog
      this.dialogRef.close(this.actorForm.value);
    }
  }

}
