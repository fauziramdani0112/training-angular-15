import { AppServiceService } from 'src/app/service/app-service.service';
import {AfterViewInit, Component, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import * as moment from 'moment';

export interface ListData {
  id: string;
  title: string;
}

@Component({
  selector: 'app-master-film',
  templateUrl: './master-film.component.html',
  styleUrls: ['./master-film.component.css'],
})
export class MasterFilmComponent {
  displayedColumns: string[] = ['id', 'title', 'release', 'update'];
  dataSource!: MatTableDataSource<ListData>;

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  @ViewChild(MatSort)
  sort!: MatSort;

  constructor(
    private apiSevice:AppServiceService
  ){}
  ngOnInit(): void {
    this.apiSevice.getAllFilms().subscribe((response:any)=>{
      if (response.status === true) {
        this.dataSource = new MatTableDataSource(response.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      } else {
        this.dataSource = new MatTableDataSource();
      }
    })
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  convertDate(date: any) {
    if (date != null) {
      return moment(date).format('LLL');
    }else{
      return date
    }
  }
}
