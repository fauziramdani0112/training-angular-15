import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterFilmComponent } from './master-film.component';

describe('MasterFilmComponent', () => {
  let component: MasterFilmComponent;
  let fixture: ComponentFixture<MasterFilmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MasterFilmComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MasterFilmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
