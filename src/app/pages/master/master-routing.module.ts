import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MasterActorComponent } from './master-actor/master-actor.component';
import { MasterFilmComponent } from './master-film/master-film.component';
import { MasterCategoryComponent } from './master-category/master-category.component';

const routes: Routes = [
    { path:'actor',component:MasterActorComponent },
    { path:'category',component:MasterCategoryComponent },
    { path:'film',component:MasterFilmComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MasterRoutingModule { }
