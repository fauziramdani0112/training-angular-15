import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterLanguageComponent } from './master-language.component';

describe('MasterLanguageComponent', () => {
  let component: MasterLanguageComponent;
  let fixture: ComponentFixture<MasterLanguageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MasterLanguageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MasterLanguageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
