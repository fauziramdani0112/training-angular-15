import { Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import * as moment from 'moment';
import { AppServiceService } from 'src/app/service/app-service.service';

export interface ListData {
  id: string;
}

@Component({
  selector: 'app-master-category',
  templateUrl: './master-category.component.html',
  styleUrls: ['./master-category.component.css']
})
export class MasterCategoryComponent {
  displayedColumns: string[] = ['id', 'name', 'update', 'action'];
  dataSource!: MatTableDataSource<ListData>;

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  @ViewChild(MatSort)
  sort!: MatSort;

  constructor(
    private apiSevice:AppServiceService,
    public dialog: MatDialog
  ){}
  ngOnInit(): void {
    this.apiSevice.getAllCategory().subscribe((response:any)=>{
      if (response.status === true) {
        this.dataSource = new MatTableDataSource(response.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      } else {
        this.dataSource = new MatTableDataSource();
      }
    })
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  convertDate(date: any) {
    if (date != null) {
      return moment(date).format('LLL');
    }else{
      return date
    }
  }
}
