import { AppServiceService } from 'src/app/service/app-service.service';
import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import * as moment from 'moment';
import { MatDialog } from '@angular/material/dialog';
import { ActorFormComponent } from '../actor-form/actor-form.component';
import Swal from 'sweetalert2';

export interface ListData {
  actor_id: string;
  first_name: string;
  last_name: string;
}
@Component({
  selector: 'app-master-actor',
  templateUrl: './master-actor.component.html',
  styleUrls: ['./master-actor.component.css']
})
export class MasterActorComponent implements OnInit {
  displayedColumns: string[] = ['id', 'first', 'last', 'update', 'action'];
  dataSource!: MatTableDataSource<ListData>;

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  @ViewChild(MatSort)
  sort!: MatSort;

  constructor(
    private apiSevice:AppServiceService,
    public dialog: MatDialog
  ){}
  
  ngOnInit(): void {
    this.apiSevice.getAllActor().subscribe((response:any)=>{
      if (response.status === true) {
        this.dataSource = new MatTableDataSource(response.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      } else {
        this.dataSource = new MatTableDataSource();
      }
    })
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  convertDate(date: any) {
    if (date != null) {
      return moment(date).format('LLL');
    }else{
      return date
    }
  }

  openDialog(item: any = null){
    const dialogRef = this.dialog.open(ActorFormComponent, {
      data: item
    });
  
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (item) {
            this.apiSevice.updateActor(item.actor_id,result).subscribe((response:any)=>{
            if (response.status === true) {
              Swal.fire(
                'Great !',
                'Update data success',
                'success'
              ).then(result =>{
                this.ngOnInit()
              })
            } else {
              Swal.fire({
                icon: 'warning',
                title: 'Oops...',
                text: 'Something went wrong!'
              })
            }
          })
        } else {
            this.apiSevice.insertActor(result).subscribe((response:any)=>{
            if (response.status === true) {
              Swal.fire(
                'Great !',
                'Add data success',
                'success'
              ).then(result =>{
                this.ngOnInit()
              })
            } else {
              Swal.fire({
                icon: 'warning',
                title: 'Oops...',
                text: 'Something went wrong!'
              })
            }
          })
        }
      }
    });
  }

  deleteData(id:number){
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!"
    }).then((result) => {
      if (result.isConfirmed) {
        this.apiSevice.updateActor(id,{is_deleted:1}).subscribe((response:any)=>{
          if (response.status === true) {
            Swal.fire(
              'Great !',
              'Delete data success',
              'success'
            ).then(result =>{
              this.ngOnInit()
            })
          } else {
            Swal.fire({
              icon: 'warning',
              title: 'Oops...',
              text: 'Something went wrong!'
            })
          }
        })
      }
    });
    
  }
}
